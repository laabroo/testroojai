package lbr.id.testroojai.repository;

import lbr.id.testroojai.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Person, Integer> {

    Optional<Person> findByMobile(String mobile);

}
