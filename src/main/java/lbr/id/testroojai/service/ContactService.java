package lbr.id.testroojai.service;

import jakarta.transaction.Transactional;
import lbr.id.testroojai.entity.ContactNotFoundException;
import lbr.id.testroojai.entity.Person;
import lbr.id.testroojai.exception.DataDuplicateException;
import lbr.id.testroojai.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Transactional
public class ContactService {

    @Autowired
    ContactRepository repo;

    public ContactService(ContactRepository repo) {
        this.repo = repo;
    }

    public Person add(Person person) {
        if(person != null) {
            this.repo.findByMobile(person.getMobile()).ifPresent(p -> {
                    throw new DataDuplicateException(p.getMobile() +" telah tercatat dalam database.");
            });
            return repo.save(person);
        } else {
            return null;
        }
    }

    public Person  findOne(int id) {
        return repo.findById(id).orElseThrow(() -> new ContactNotFoundException("contact dengan id ["+id+"] tidak ditemukan"));
    }

    public Optional<Person>  findByMobile(String mobile) {
        Optional<Person> pDb = repo.findByMobile(mobile);
        return pDb;
    }

}
