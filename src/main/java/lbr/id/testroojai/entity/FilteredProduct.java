package lbr.id.testroojai.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class FilteredProduct implements Serializable {
    @JsonProperty
    @JsonIgnore
    private String item;
    @JsonProperty
    @JsonIgnore
    private String category;
    @JsonProperty
    @JsonIgnore
    private int available;
    @JsonProperty("barCode")
    private String barcode;
    @JsonProperty
    @JsonIgnore
    private int price;

    @JsonProperty
    @JsonIgnore
    private float discount;

    public FilteredProduct() {
    }

    public FilteredProduct(String item, String category, int available, String barcode, int price, float discount) {
        this.item = item;
        this.category = category;
        this.available = available;
        this.barcode = barcode;
        this.price = price;
        this.discount = discount;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }
}
