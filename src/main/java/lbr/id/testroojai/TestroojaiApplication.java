package lbr.id.testroojai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestroojaiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestroojaiApplication.class, args);
	}

}
