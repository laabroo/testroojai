package lbr.id.testroojai.exception;

import lbr.id.testroojai.entity.ContactNotFoundException;
import lbr.id.testroojai.entity.CustomResponseError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomControllerAdvice extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        CustomResponseError err = new CustomResponseError();
        err.setMessage("Please check your input");
        err.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataDuplicateException.class)
    public ResponseEntity<CustomResponseError> handleDataDuplicateException(DataDuplicateException e) {
        CustomResponseError err = new CustomResponseError();
        err.setMessage(e.getMessage());
        err.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ContactNotFoundException.class)
    public ResponseEntity<CustomResponseError> handleContactNotFoundException(ContactNotFoundException e) {
        CustomResponseError err = new CustomResponseError();
        err.setMessage(e.getMessage());
        err.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
    }

}
