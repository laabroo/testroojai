package lbr.id.testroojai.controller;

import lbr.id.testroojai.entity.Person;
import lbr.id.testroojai.exception.CustomControllerAdvice;
import lbr.id.testroojai.repository.ContactRepository;
import lbr.id.testroojai.service.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

@RestController
public class ContactController {

    @Autowired
    ContactService contactService;
    private Logger log = LoggerFactory.getLogger(ContactController.class);

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @CrossOrigin
    @PostMapping("/contact/save")
    private ResponseEntity<Person> store(@RequestBody Person person) {
        Person p = contactService.add(person);
        return new ResponseEntity<Person>(p, HttpStatus.CREATED);
    }

    @CrossOrigin
    @GetMapping("/contact/retrieve/{id}")
    private ResponseEntity<Person> retrieve(@PathVariable("id") int id) {
        var item = contactService.findOne(id);
        return new ResponseEntity<Person>(item, HttpStatus.OK);
    }
}
