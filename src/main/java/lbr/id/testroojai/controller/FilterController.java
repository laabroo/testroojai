package lbr.id.testroojai.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import lbr.id.testroojai.entity.FilteredProduct;
import lbr.id.testroojai.entity.SortedProduct;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

@RestController
public class FilterController {

    final String uri = "https://jsonmock.hackerrank.com/api/inventory";
    RestTemplate restTemplate = new RestTemplate();
    String result = restTemplate.getForObject(uri, String.class);
    JSONObject root = new JSONObject(result);

    JSONArray data = root.getJSONArray("data");

    @CrossOrigin
    @GetMapping("/filter/price/{initial_price}/{final_price}")
    private ResponseEntity<ArrayList<FilteredProduct>> filtered_books(@PathVariable("initial_price") int init_price , @PathVariable("final_price") int final_price)
    {
        try {
            ArrayList<FilteredProduct> books = new ArrayList<FilteredProduct>();
            for(int a =0; a< data.length(); a++) {
                JSONObject obj = data.getJSONObject(a);
                FilteredProduct fp = new FilteredProduct();
                fp.setBarcode(obj.getString("barcode"));
                fp.setPrice(obj.getInt("price"));

//                fp.setAvailable(obj.getInt("available"));
//                fp.setItem(obj.getString("item"));
//                fp.setCategory(obj.getString("category"));
//                fp.setDiscount(obj.getFloat("discount"));

                if(fp.getPrice() >= init_price && fp.getPrice() <= final_price) {
                    books.add(fp);
                }
            }

            if(books.size() > 0) {
                return new ResponseEntity<ArrayList<FilteredProduct>>(books, HttpStatus.OK);
            } else {
                return new ResponseEntity<ArrayList<FilteredProduct>>(books, HttpStatus.BAD_REQUEST);
            }
        }catch(Exception e)
        {
            System.out.println("Error encountered : "+e.getMessage());
            return new ResponseEntity<ArrayList<FilteredProduct>>(HttpStatus.NOT_FOUND);
        }

    }


    @CrossOrigin
    @GetMapping("/sort/price")
    private ResponseEntity<SortedProduct[]> sorted_books()
    {

        try {

            SortedProduct[] ans=new SortedProduct[data.length()];
            for(int a =0; a< data.length(); a++) {
                JSONObject obj = data.getJSONObject(a);
                SortedProduct fp = new SortedProduct();
                fp.setBarcode(obj.getString("barcode"));
                fp.setPrice(obj.getInt("price"));

//                fp.setAvailable(obj.getInt("available"));
//                fp.setItem(obj.getString("item"));
//                fp.setCategory(obj.getString("category"));
//                fp.setDiscount(obj.getFloat("discount"));

                ans[a] = fp;
            }

            if(ans.length > 0) {
                Arrays.sort(ans, new Comparator<SortedProduct>() {
                    @Override
                    public int compare(SortedProduct o1, SortedProduct o2) {
                        if (o1.getPrice() != o2.getPrice()) {
                            return Integer.valueOf(o1.getPrice()).compareTo(o2.getPrice());
                        }
                        return 0;
                    }
                });

                return new ResponseEntity<SortedProduct[]>(ans, HttpStatus.OK);
            } else {
                return new ResponseEntity<SortedProduct[]>(ans, HttpStatus.BAD_REQUEST);
            }

        }catch(Exception E)
        {
            System.out.println("Error encountered : "+E.getMessage());
            return new ResponseEntity<SortedProduct[]>(HttpStatus.NOT_FOUND);
        }

    }

}
