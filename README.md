# TestRoojai



### Soal Nomor 1:
#### A. Menampilkan data berdasarkan kisaran harga (price)

GET localhost:8080/filter/price/{initial_range}/{final_range}

GET localhost:8080/filter/price/750/785

Dari requet ini akan menampilkan 4 data sebagai berikut:

HTTP_STATUS_CODE = 200 (OK)


```
[
    {
    "barCode": "74001755"
    },
    {
    "barCode": "74000964"
    },
    {
    "barCode": "74000246"
    },
    {
    "barCode": "74001440"
    }
]
```

#### B. Menampilkan data yang berurut berdasarkan harga (price) mulai dari terkecil
GET localhost:8080/sort/price

HTTP_STATUS_CODE = 200 (OK)


```
[
    {
    "barCode": "47000395"
    },
    {
    "barCode": "74001729"
    },
    {
    "barCode": "47000380"
    },
    {
    "barCode": "47000256"
    },
    .
    .
    .
    .
    .
    .
    {
        "barCode": "74001845"
    },
    {
        "barCode": "74001967"
    },
    {
        "barCode": "74001177"
    },
    {
        "barCode": "74000886"
    }
]
```

### Soal Nomor 2:
#### A. Menyimpan data kontak dengan variable name dan mobile

POST localhost:8080/contact/save

Contoh data json yang di kirim:
```
{
 "name": "Budi",
 "mobile": "08123123123"
}
```
Jika berhasil, maka akan memunculkan data contact kurang lebih sebagai berikut:

HTTP_STATUS_CODE = 201 (CREATED)


```
{
 "id": 1,
 "name": "Budi",
 "mobile": "08123123123"
}
```

#### B. Menampilkan data kontak berdasarkan ID
GET localhost:8080/contact/retrieve/1

Jika data ditemukan, maka akan menghasilkan response json sebagai berikut:

HTTP_STATUS_CODE = 200 (OK)
```
{
 "id": 1,
 "name": "Budi",
 "mobile": "08123123123"
}
```

Namun jika data tidak ditemukan maka akan menghasilkan response sebagai berikut:

HTTP_STATUS_CODE = 400 (BAD_REQUEST)

```
{
    "status": "Bad Request",
    "message": "contact dengan id [11] tidak ditemukan"
}
```